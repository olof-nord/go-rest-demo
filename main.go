package main

import (
    "net/http"
    "gitlab.com/olof-nord/go-rest-demo/web"
)

func main() {
    http.HandleFunc("/users", web.UsersHandler)
    http.HandleFunc("/users/", web.UserHandler)

    http.ListenAndServe(":8080", nil)
}
