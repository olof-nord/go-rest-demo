package web

import (
    "strings"
    "net/http"
    "encoding/json"
)

type User struct {
    FirstName  string `json:"firstName"`
    LastName string `json:"lastName"`
}

func UsersHandler(w http.ResponseWriter, r *http.Request) {
    if r.Method == http.MethodPost {
        decoder := json.NewDecoder(r.Body)

        var response User
        err := decoder.Decode(&response)
        if err != nil {
            panic(err)
        }

        if response.FirstName == "" || response.LastName == "" {
            w.WriteHeader(http.StatusUnprocessableEntity)
            return
        }

        user := User{response.FirstName, response.LastName}
        json.NewEncoder(w).Encode(user)

    } else if r.Method == http.MethodGet {
        users := []User{
            User{"John", "Doe"},
            User{"Jane", "Doe"},
        }
        json.NewEncoder(w).Encode(users)

    } else {
        w.WriteHeader(http.StatusMethodNotAllowed)
    }
}

func UserHandler(w http.ResponseWriter, r *http.Request) {
    id := strings.TrimPrefix(r.URL.Path, "/users/")

    if r.Method == http.MethodGet {
        // It is a demo after all
        if(id == "1") {
            user := User{"John", "Doe"}
            json.NewEncoder(w).Encode(user)
        } else {
            w.WriteHeader(http.StatusNotFound)
        }
    } else {
        w.WriteHeader(http.StatusMethodNotAllowed)
    }
}
