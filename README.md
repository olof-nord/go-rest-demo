# go-rest-demo

Demo project for a simple GET and POST REST server written with golang.

## Dependencies

This demo requires `go` to be installed locally.

## Start

To run the program with go directly:
`go run .`

See the Open API [spec](go-rest-demo.openapi.yaml) for the full details.

## Example output

```shell
~ curl -s http://localhost:8080/users | jq .
[
    {
        "firstName": "John",
        "lastName": "Doe"
    },
    {
        "firstName": "Jane",
        "lastName": "Doe"
    }
]
```

```shell
~ curl -s http://localhost:8080/users/1 | jq .
{
    "firstName": "John",
    "lastName": "Doe"
}
```

```shell
~ curl -s -D - -o /dev/null http://localhost:8080/users/2
HTTP/1.1 404 Not Found
```

Repository icon made by [Freepik](https://www.freepik.com) found at [Flaticon](https://www.flaticon.com).
